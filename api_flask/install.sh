#!/bin/sh

ZBOT_SRC_PREFIX=/opt/zidane_api
ZBOT_SYMLINK=/usr/bin/zidane-api
ZBOT_SERVICE_PREFIX=/etc/systemd/system
SCRIPT_PWD=$(realpath ${0})
SCRIPT_DIR=$(dirname ${SCRIPT_PWD})

install_venv() {
    python3 -m virtualenv ${ZBOT_SRC_PREFIX}/venv
    . ${ZBOT_SRC_PREFIX}/venv/bin/activate
    pip install -r ${ZBOT_SRC_PREFIX}/requirements.txt
}

install_sources() {
    mkdir -p ${ZBOT_SRC_PREFIX}
    cp ${SCRIPT_DIR}/*.py ${ZBOT_SRC_PREFIX}
    cp ${SCRIPT_DIR}/run.sh ${ZBOT_SRC_PREFIX}
    cp ${SCRIPT_DIR}/requirements.txt ${ZBOT_SRC_PREFIX}
    chowm -R ${USER}:${USER} ${ZBOT_SRC_PREFIX}
}

install_links() {
    chmod u+x ${ZBOT_SRC_PREFIX}/run.sh
    ln -s ${ZBOT_SRC_PREFIX}/run.sh ${ZBOT_SYMLINK}
    chown ${USER}:${USER} ${ZBOT_SYMLINK}
}

install_service() {
    cp ${SCRIPT_DIR}/zidane-api.service ${ZBOT_SERVICE_PREFIX}/zidane-api.service
    systemctl enable zidane-api.service
}

install_api() {
    install_sources
    install_venv
    install_links
    install_service
}

remove_api() {
    rm -rf ${ZBOT_SRC_PREFIX}
    rm -rf ${ZBOT_SYMLINK}
    systemctl stop zidane-api.service
    systemctl disable zidane-api.service
    rm -f ${ZBOT_SERVICE_PREFIX}/zidane-api.service
}

update_api() {
    install_sources
}

test "$(/usr/bin/id -u)" != "0" && echo "Run as sudo or root please" && exit 1;

while getopts "iru" option
do
    case ${option} in
        i) install_api ;;
        r) remove_api ;;
        u) update_api ;;
    esac
done

