#!/usr/bin/python3

from flask import Flask, jsonify, request
from file_manager import FileManager


app = Flask(__name__)

@app.route('/file', methods=['GET'])
def get_all_file():
    return FileManager.get_all_file()


@app.route('/lines', methods=['GET'])
def get_lines():
    lines = FileManager.file_to_array()
    lines_json = { 'lines': lines }
    return jsonify(lines_json)


@app.route('/file', methods=['POST'])
def add_to_file():
    return ''


@app.route('/file', methods=['DELETE'])
def delete_line():
    return ''


if __name__ == '__main__':
    app.run(port=8000)
