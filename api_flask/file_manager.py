FILENAME = "/usr/share/zidane_bot/quotes.txt"

class FileManager():

    @staticmethod
    def get_all_file():
        with open(FILENAME, "r") as quotes:
            return quotes.read()

    @staticmethod
    def file_to_array():
        with open(FILENAME, "r") as quotes:
            return quotes.readlines()

    @staticmethod
    def append_to_file(line):
        with open(FILENAME, "a") as quotes:
            quotes.write(line)

    @staticmethod
    def delete_line(line):
        with open(FILENAME, "r+") as quotes:
            content = quotes.readlines()
            content.remove(line)
            quotes.writelines(content)

