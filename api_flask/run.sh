#!/bin/sh

readonly script_pwd=$(realpath ${0})
readonly script_folder=$(dirname ${script_pwd})
. ${script_folder}/venv/bin/activate
python ${script_folder}/app.py

