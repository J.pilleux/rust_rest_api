import pathlib
import random
import os
from dal import ZidaneQuoteDal

CWD = pathlib.Path().absolute()


def get_random_quote():
    dal = ZidaneQuoteDal()
    zidane_quotes = dal.get_all_quotes()
    return random.choice(zidane_quotes)


def get_bonjour():
    return os.path.join(CWD, "/sounds/bonjour.mp3")


def get_parfait():
    return os.path.join(CWD, "/sounds/parfait.mp3")

