#!/bin/sh

ZBOT_SRC_PREFIX=/opt/zidane_bot
ZBOT_SYMLINK=/usr/bin/zidane-bot
ZBOT_SERVICE_PREFIX=/etc/systemd/system
SCRIPT_PWD=$(realpath ${0})
SCRIPT_DIR=$(dirname ${SCRIPT_PWD})

install_venv() {
    python3 -m virtualenv ${ZBOT_SRC_PREFIX}/venv
    . ${ZBOT_SRC_PREFIX}/venv/bin/activate
    pip install -r ${ZBOT_SRC_PREFIX}/requirements.txt
}

install_sources() {
    mkdir -p ${ZBOT_SRC_PREFIX}
    cp ${SCRIPT_DIR}/*.py ${ZBOT_SRC_PREFIX}
    cp ${SCRIPT_DIR}/.env ${ZBOT_SRC_PREFIX}
    cp ${SCRIPT_DIR}/requirements.txt ${ZBOT_SRC_PREFIX}
    cp ${SCRIPT_DIR}/run.sh ${ZBOT_SRC_PREFIX}
    cp -r ${SCRIPT_DIR}/sounds ${ZBOT_SRC_PREFIX}
    chown -R ${USER}:${USER} ${ZBOT_SRC_PREFIX}
}

install_links() {
    chmod u+x ${ZBOT_SRC_PREFIX}/run.sh
    ln -s ${ZBOT_SRC_PREFIX}/run.sh ${ZBOT_SYMLINK}
    chown ${USER}:${USER} ${ZBOT_SYMLINK}
}

install_service() {
    cp ${SCRIPT_DIR}/zidane-bot.service ${ZBOT_SERVICE_PREFIX}/zidane-bot.service
    systemctl enable zidane-bot.service
}

install_zidane() {
    install_sources
    install_venv
    install_links
    install_service
}

remove_zidane() {
    rm -rf ${ZBOT_SRC_PREFIX}
    rm -f ${ZBOT_SYMLINK}
    systemctl stop zidane-bot.service
    systemctl disable zidane-bot.service
    rm -f ${ZBOT_SERVICE_PREFIX}/zidane-bot.service
}

update_zidane() {
    install_sources
}

test "$(/usr/bin/id -u)" != "0" && echo "Run as sudo or root please" && exit 1;

while getopts "iru" option
do
    case ${option} in
        i) install_zidane ;;
        r) remove_zidane ;;
        u) update_zidane ;;
    esac
done

