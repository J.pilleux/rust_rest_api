def get_channel_action_message(member, before, after):
    msg = None
    if _is_user_start_streaming(before, after):
        msg = "Oh mon dieu ! {} s'est mit à streamer ! Venez vite voir, ça " \
            "me donne envie de jouer au foot !"
    elif _is_user_stop_steaming(before, after):
        msg = "Le stream de {} est terminé. Dommage c'était top !"
    return None if msg is None else msg.format(_get_member_name(member))


def _is_user_start_streaming(before, after):
    return not before.self_stream and after.self_stream


def _is_user_stop_steaming(before, after):
    return before.self_stream and not after.self_stream


def _get_member_name(member):
    return member.nick if member.nick is not None else member.display_name
