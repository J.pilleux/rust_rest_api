import requests


class ZidaneQuoteDal():

    def get_all_quotes(self):
        headers = {"content-type": "application/json"}
        lines = {}
        try:
            response = requests.get("http://127.0.0.1:8000/lines", headers=headers)
            lines = response.json()
        except requests.exceptions.ConnectionError:
            lines["lines"] = ["J'aime jouer au foot"]

        return lines.get("lines", [])
