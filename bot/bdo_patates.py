import os
import math

MIN_PATATES = 6
PATATE = 5
LEAVING = 2
SUGAR = 1
WATER = 6


class PatateError(Exception):
    pass


class Beer:
    def __init__(self, nbPatates):
        if nbPatates < MIN_PATATES:
            raise PatateError
        self._count = math.floor(nbPatates / PATATE)
        self._patate = self._count * PATATE
        self._leaving = self._count * LEAVING
        self._sugar = self._count * SUGAR
        self._water = self._count * WATER

    def __str__(self):
        string = "To make " + str(self._count) + " beers, you need :" + os.linesep \
            + "Patate :potato: : " + str(self._patate) + os.linesep \
            + "Leaving : " + str(self._leaving) + os.linesep \
            + "Sugar : " + str(self._sugar) + os.linesep \
            + "Water : " + str(self._water) + os.linesep
        return string
