from dotenv import load_dotenv
from bot import run_bot
import getopt
import os
import sys

load_dotenv()


def is_debug_from_getopt():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "d", ["debug"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    debug = False
    for opt, _ in opts:
        if opt in ("-d", "--debug"):
            debug = True
        else:
            assert False, "Unhandled option"
    return debug


def main():
    if is_debug_from_getopt():
        run_bot(os.getenv("TOKEN_BOT_DEBUG"),
                int(os.getenv("TOKEN_CHANNEL_DEBUG")))
    else:
        run_bot(os.getenv("TOKEN_BOT_PROD"),
                int(os.getenv("TOKEN_CHANNEL_PROD")))


if __name__ == "__main__":
    main()
