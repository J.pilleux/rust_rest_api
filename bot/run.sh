#!/bin/sh

ARGS=

while getopts "d" option; do
    case ${option} in
        d) ARGS=-d;;
        ?) echo "Invalid option"; exit 1;;
    esac
done

readonly script_pwd=$(realpath ${0})
readonly script_folder=$(dirname ${script_pwd})
. ${script_folder}/venv/bin/activate
python ${script_folder}/main.py ${ARGS}

