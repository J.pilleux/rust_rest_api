from discord import FFmpegPCMAudio
from discord.ext import commands
from discord.utils import get
from voice_state import get_channel_action_message
from zidane_msg import get_random_quote
from bdo_patates import Beer, PatateError
import random
import os
import time


client = commands.Bot(command_prefix='!')
dir_path = os.path.dirname(os.path.realpath(__file__))


async def write_in_general(message):
    channel = client.get_channel(general_channel)
    await channel.send(message)


@client.event
async def on_member_join(member):
    msg = 'Salut {member}, tu aimes jouer au foot ?'
    await client.send_message("general", msg)


@client.event
async def on_voice_state_update(member, before, after):
    msg = get_channel_action_message(member, before, after)
    if msg is not None:
        await write_in_general(msg)


@client.command(name='rnd')
async def random_game(ctx, *args):
    if args:
        game = random.choice(args)
        await write_in_general(game)
    else:
        await write_in_general("Faut mettre des mots après mongolo")


@client.command(name='patates')
async def bdo_patates(ctx, *args):
    if args and len(args) == 1:
        try:
            nbPatates = int(args[0])
            beer = Beer(nbPatates)
            await write_in_general(str(beer))
        except ValueError:
            await write_in_general("Ce n'est pas un nombre de patate ça !")
        except PatateError:
            await write_in_general("Il te faut au moins 6 patates !")
    else:
        await write_in_general("HELP: !patates <nbPatates>")


@client.command(name='fs', pass_context=True)
async def say_tagueule_short(context):
    await say_tagueule(context)


@client.command(name='forceskip', pass_context=True)
async def say_tagueule(context):
    channel = context.message.author.voice.channel
    if not channel:
        return
    voice = get(client.voice_clients, guild=context.guild)
    if voice and voice.is_connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()
    source = FFmpegPCMAudio(dir_path + "/sounds/tg.mp3")
    player = voice.play(source)
    time.sleep(10)
    await voice.disconnect()


@client.event
async def on_message(message):
    if 'zidane' in message.content.lower():
        msg = get_random_quote()
        await message.channel.send(msg)
    await client.process_commands(message)


@client.event
async def on_connect():
    print("Connected")


@client.event
async def on_disconnect():
    print("Disconnected")


def run_bot(token, channel):
    bot_token = token
    global general_channel
    general_channel = channel
    client.run(bot_token)
