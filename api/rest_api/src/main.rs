use actix_cors::Cors;
use actix_web::{http, delete, get, post, App, HttpResponse, HttpServer};
use serde::{Serialize, Deserialize};

extern crate file_manager;

use file_manager::file_manager::*;

#[derive(Serialize, Deserialize)]
struct Lines {
    lines: Vec<String>,
}

#[get("/file")]
async fn get_all_file() -> HttpResponse {
    let file_content = file_io::get_all_file();
    HttpResponse::Ok()
        .header(http::header::ACCESS_CONTROL_ALLOW_ORIGIN, "*")
        .content_type("plain/text").body(file_content)
}

#[get("/lines")]
async fn get_lines() -> HttpResponse {
    let file_lines = file_io::file_to_array();
    let lines = Lines{lines: file_lines};
    HttpResponse::Ok()
        .content_type("application/json")
        .json2(&lines)
}

#[post("/file")]
async fn add_to_file(req_body: String) -> HttpResponse {
    if req_body.is_empty() {
        HttpResponse::BadRequest().body("Can't add an empty quote")
    } else {
        file_io::append_to_file(&req_body);
        let msg: String = std::format!("Line {} sucessfully added", req_body);
        HttpResponse::Ok().body(msg)
    }
}

#[delete("/file")]
async fn delete_line(req_body: String) -> HttpResponse {
    let message: String;
    match file_io::delete_line(&req_body) {
        Ok(msg) => message = msg,
        Err(err) => message = err,
    }
    HttpResponse::Ok().body(message)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    return HttpServer::new(|| {
        let cors = Cors::permissive();

        App::new()
            .wrap(cors)
            .service(get_all_file)
            .service(add_to_file)
            .service(delete_line)
            .service(get_lines)
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await;
}
