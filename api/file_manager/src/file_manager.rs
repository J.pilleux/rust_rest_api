pub mod file_io {

    use std:: {
        fs::{OpenOptions, File},
        io::{prelude::*, BufReader}
    };

    use std::fs;

    const FILENAME: &str = "/usr/share/zidane_bot/quotes.txt";

    pub fn get_all_file() -> String {
        let mut file = File::open(FILENAME).unwrap();
        let mut content = String::new();
        file.read_to_string(&mut content).unwrap();
        return content;
    }

    pub fn file_to_array() -> Vec<String> {
        let file = File::open(FILENAME).unwrap();
        let buf = BufReader::new(file);
        buf.lines().map(|l| l.expect("Could not parse line")).collect()
    }

    pub fn append_to_file(line: &str) {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(FILENAME)
            .unwrap();

        if let Err(e) = writeln!(file, "{}", line) {
            eprintln!("Could not write to file : {}", e);
        }
    }

    pub fn delete_line(line: &str) -> Result<String, String> {
        let mut content: Vec<String> = file_to_array();

        if ! content.contains(&String::from(line)) {
            let msg: String = std::format!("Line {} not found in {}", line, FILENAME);
            return Err(msg);
        }

        content.retain(|x| x != line);
        let mut new_content = String::new();
        for l in content {
            new_content += &String::from(l + "\n");
        }

        fs::write(FILENAME, &new_content).unwrap();
        return Ok(new_content);
    }
}
