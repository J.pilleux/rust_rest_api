const LIST_CONTAINER_ID = 'list-container';
const ERR_CONTAINER_ID = 'error-container';

function clear_element_children(parent) {
    while (parent.lastChild) {
        parent.removeChild(parent.lastChild);
    }
};

function clear_element(element_id) {
    const element = document.getElementById(element_id);
    clear_element_children(element);
}

function clear_all() {
    clear_element(LIST_CONTAINER_ID);
    clear_element(ERR_CONTAINER_ID);
}

function write_content(element_id, content) {
    const element = document.getElementById(element_id);
    if (element !== null) {
        element.innerHTML = content;
    } else {
        console.error('The element', element_id, 'does not exists');
    }
}

function append_to_element(element_id, element_to_append) {
    const element = document.getElementById(element_id);
    if (element !== null) {
        element.appendChild(element_to_append);
    } else {
        console.error('The element', element_id, 'does not exists');
    }
}

function create_list(list_content, element_id) {
    let list = document.createElement('ul');
    const list_len = list_content.length;
    for (let i = 0; i < list_len; ++i) {
        list.appendChild(create_list_item(list_content[i]));
    }
    return list;
}

function create_sentense(text) {
    const span = document.createElement('span');
    const paragraph = document.createElement('a');
    paragraph.innerHTML = text;
    button = create_delete_btn(text);
    span.appendChild(button);
    span.appendChild(paragraph);
    return span;
}

function create_delete_btn(item) {
    const button = document.createElement('button');
    button.classList.add('delete-btn');
    button.innerHTML = '🞬';
    button.onclick = () => { delete_list_item(item) };
    return button;
}

function create_list_item(item) {
    const list_item = document.createElement('li');
    const span = create_sentense(item);
    list_item.appendChild(span);
    return list_item;
}

function create_error_msg(text) {
    const div = document.createElement('div');
    const head = document.createElement('h2');
    div.classList = 'error-box';
    head.classList.add('error-txt');
    const paragraph = document.createElement('p');
    head.innerHTML = 'ERROR';
    paragraph.innerHTML = text;
    div.appendChild(head);
    div.appendChild(paragraph);
    return div;
}

function display_error(text) {
    const element = create_error_msg(text);
    append_to_element(ERR_CONTAINER_ID);
}

async function add_new_quote() {
    const input = document.getElementById('quote-input');
    const quote = input.value;
    const headers = new Headers();
    headers.append('Content-Type', 'plain/text');
    const init = {
        method: 'POST',
        mode: 'cors',
        body: quote
    }
    fetch('http://localhost:8000/file', init)
    .then(response => {
        if (! response.ok) {
            throw new Error(dispatch(response));
        }
        return response.text()
    })
    .then(text => { get_list() })
    .catch(err => { alert(err) });
}

async function delete_list_item(item) {
    const headers = new Headers();
    headers.append('Content-Type', 'plain/text');
    const init = {
        method: 'DELETE',
        mode: 'cors',
        body: item
    }
    fetch('http://localhost:8000/file', init)
    .then(response => { return response.text() })
    .then(text => { get_list() })
    .catch(err => { display_error(err) });
}

async function get_list() {
    clear_all(LIST_CONTAINER_ID);
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    const init = {
        method: 'GET',
        headers: headers,
        mode: 'cors'
    }
    fetch('http://localhost:8000/lines', init)
    .then(response => { return response.json() })
    .then(json => {
        const header = document.createElement('h2');
        header.innerHTML = 'Zidane quote list'
        const list = create_list(json.lines);
        append_to_element(LIST_CONTAINER_ID, header);
        append_to_element(LIST_CONTAINER_ID, list);
    })
    .catch(err => {
        display_error(err);
    });
}

get_list();

