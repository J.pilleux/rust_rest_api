#!/bin/sh

readonly DEPLOY_PREFIX=/tmp/zidane

test "${#}" != "1" && echo "Usage: deploy.sh user@hostname" && exit 1

DEST=${1}

msg() {
    local msg=${1}
    echo "-- ${msg}"
}

deploy_bot(){
    msg "Deploying bot sources"
    ssh -q ${DEST} mkdir -p ${DEPLOY_PREFIX}/bot
    rsync bot/*.py ${DEST}:${DEPLOY_PREFIX}/bot
    rsync bot/*.sh ${DEST}:${DEPLOY_PREFIX}/bot
    rsync bot/requirements.txt ${DEST}:${DEPLOY_PREFIX}/bot
    rsync bot/.env ${DEST}:${DEPLOY_PREFIX}/bot
    rsync bot/zidane-bot.service ${DEST}:${DEPLOY_PREFIX}/bot
    rsync -r bot/sounds ${DEST}:${DEPLOY_PREFIX}/bot
}

deploy_flask_api() {
    msg "Deploying flask api sources"
    ssh -q ${DEST} mkdir -p ${DEPLOY_PREFIX}/api_flask
    rsync api_flask/*.py ${DEST}:${DEPLOY_PREFIX}/api_flask
    rsync api_flask/*.sh ${DEST}:${DEPLOY_PREFIX}/api_flask
    rsync api_flask/requirements.txt ${DEST}:${DEPLOY_PREFIX}/api_flask
    rsync api_flask/zidane-api.service ${DEST}:${DEPLOY_PREFIX}/api_flask
}

main() {
    ssh -q ${DEST} mkdir -p ${DEPLOY_PREFIX}
    deploy_bot
    deploy_flask_api
}

main
